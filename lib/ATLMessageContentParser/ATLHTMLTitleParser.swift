//
//  ATLHTMLTitleParser.swift
//  ATLMessageContentParser
//
//  Created by Maxim Malyhin on 1/13/16.
//  Copyright © 2016 Maksym Malyhin. All rights reserved.
//

import Foundation

struct ATLHTMLPageData
{
    let data: NSData!
    let contentType: String?
    
    init(data: NSData!, httpResponse: NSHTTPURLResponse)
    {
        self.data = data
        self.contentType = httpResponse.allHeaderFields["Content-Type"] as? String
    }
}

typealias ATLHTMLTitleParserCompletionHandler = (String?) -> Void

class ATLHTMLTitleParser
{
    let urlSession: NSURLSession
    let pageURL: NSURL
    let completionHandler: ATLHTMLTitleParserCompletionHandler
    let internalQueue = dispatch_queue_create("ATLHTMLTitleParser.internal", DISPATCH_QUEUE_SERIAL)

    var htmlTitleParser: ATLHTMLTitleLibXMLSAXParser?
    
    var currentDownloadTask: NSURLSessionTask?
    
    init(withPageURL url: NSURL!, urlSession: NSURLSession?, handler: ATLHTMLTitleParserCompletionHandler)
    {
        self.pageURL = url
        self.completionHandler = handler
        
        if let unwrapedURLSession = urlSession {
            self.urlSession = unwrapedURLSession
        }
        else {
            self.urlSession = NSURLSession.init(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
        }
    }
    
    func parse()
    {
        self.downloadHTMPageData {
            [weak self](optionalData: ATLHTMLPageData?) -> Void in
            
            if let htmlPageData = optionalData {
                
                if let localSelf = self {
                    
                    dispatch_async(localSelf.internalQueue, { [unowned localSelf] () -> Void in
                        
                        localSelf.htmlTitleParser = ATLHTMLTitleLibXMLSAXParser.init(withHTMLPageData: htmlPageData.data, completionHandler: { (title) -> Void in
                            
//                            localSelf.completionHandler(title)
                        })
                        
                        let title = localSelf.htmlTitleParser?.parse()
                        
                        localSelf.completionHandler(title)
                        
                    })
                }
                
                
            }
        }
    }
    
    func downloadHTMPageData(handler: (ATLHTMLPageData?) -> Void)
    {
        // TODO: Add error processing if need
        self.currentDownloadTask = self.urlSession.dataTaskWithURL(self.pageURL, completionHandler: {
           [weak self] (optionalData: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            
            if let data = optionalData {
                
                if let httpResponse = response as? NSHTTPURLResponse {
                    
                    let htmlPageData = ATLHTMLPageData.init(data: data, httpResponse: httpResponse)
                    handler(htmlPageData)
                }
            }
            
            self?.currentDownloadTask = nil
        })
        
        self.currentDownloadTask?.resume()
    }
}


