//
//  ATLMessageContentExtractor.swift
//  ATLMessageContentParser
//
//  Created by Maxim Malyhin on 1/7/16.
//  Copyright © 2016 Maksym Malyhin. All rights reserved.
//

import Foundation

// MARK: - ATLLinkInfo
public struct ATLLinkInfo : Equatable
{
    let URL: String
    let title: String?
    
    init(withURL url: String, title: String?)
    {
        self.URL = url
        self.title = title
    }
}

public func ==(lhs: ATLLinkInfo, rhs: ATLLinkInfo) -> Bool
{
    return lhs.URL == rhs.URL && lhs.title == rhs.title
}

// MARK: - ATLMessageContent

public enum ATLMessageContent : Equatable
{
    case StringContent(String)
    case Link(ATLLinkInfo)
}

public func ==(a: ATLMessageContent, b: ATLMessageContent) -> Bool
{
    switch (a, b) {
        
    case (.StringContent(let a), .StringContent(let b)): return a == b
    case (.Link(let a), .Link(let b)): return a == b
        
    case (.StringContent, _): return false
    case(.Link, _): return false
    }   
}


// MARK: - ATLMessageContentExtractorInterface
public typealias ATLContentExtractingResultHandler = ([ATLMessageContent]?) -> Void

public protocol ATLMessageContentExtractorInterface
{
    func extractContentFromString(string: String!, completionHandler:ATLContentExtractingResultHandler!)
}

// MARK: - ATLMessageContentExtractor
public class ATLMessageContentExtractor
{
    
    let contentExtractorsByKey: [String : ATLMessageContentExtractorInterface]
    let internalQueue = dispatch_queue_create("ATLMessageContentExtractor.inernal", DISPATCH_QUEUE_SERIAL)

    
    public init(withContentExtractorsByKey contentExtractorsByKey: [String : ATLMessageContentExtractorInterface])
    {
        self.contentExtractorsByKey = contentExtractorsByKey
    }
    
    /**
     * @param completionHandler converting extracted content info to JSON string is not an optimal way 
     * to pass the result. For real project I would use either a Dictionary that is ready to be encoded
     * into JSON data or JSON data that is ready to be sent to a server.
     */
    public func extractContentFromString(string: String!, completionHandler:(String?) -> Void)
    {
        // dispatch_group is used to handle complition of all content operations
        let dispatchGroup = dispatch_group_create()
        
        var contentByKey = Dictionary<String, [ATLMessageContent]>()
        
        for (key, extractor) in self.contentExtractorsByKey {
            
            dispatch_group_enter(dispatchGroup)
            
            extractor.extractContentFromString(string, completionHandler: {
                [unowned self](optionalContent: [ATLMessageContent]?) -> Void in
                
                dispatch_async(self.internalQueue, {
                    () -> Void in
                    
                    if let content = optionalContent {
                        contentByKey[key] = content
                    }
                })
                
                dispatch_group_leave(dispatchGroup)
            })
        }
        
        dispatch_group_notify(dispatchGroup, self.internalQueue) {
            () -> Void in
            
            let jsonString = ATLJSONEncoder.JSONString(withContentByKey: contentByKey)
            completionHandler(jsonString)
        }
        
    }
}

class ATLJSONEncoder
{
    static func JSONString(withContentByKey contentByKey: [String: [ATLMessageContent]]) -> String?
    {
        var primitiveTypesContentByKey: [NSString : [AnyObject]] = Dictionary.init(minimumCapacity: contentByKey.count)
        
        for (key, contents) in contentByKey {
            
            let primitiveTypesContents = contents.map({
                (content) -> AnyObject in
                
                switch content {
                    
                case .StringContent(let contentString):
                    return contentString
                    
                case .Link(let linkInfo):
                    if let title = linkInfo.title {
                        return ["url" : linkInfo.URL, "title" : title]
                    }
                    else {
                        return ["url" : linkInfo.URL]
                    }
                }
            })
            
            primitiveTypesContentByKey[key] = primitiveTypesContents
        }

        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(primitiveTypesContentByKey, options:.PrettyPrinted)
            
            let jsonString = String.init(data: jsonData, encoding: NSUTF8StringEncoding)
            
            // Currently symbols "/" are converted to "\/" during JSON serialization to NSData and converting
            // NSData back to string. Let's fix this issue using brute force for now. In real production code there
            // should not be converting like this, because result should be either primitiveTypesContentByKey or
            // jsonData.
            
            let prettyJSONString = jsonString?.stringByReplacingOccurrencesOfString("\\/", withString: "/")
            
            return prettyJSONString
        }
        catch {
            
        }
        
        return nil
    }
    
    
}
