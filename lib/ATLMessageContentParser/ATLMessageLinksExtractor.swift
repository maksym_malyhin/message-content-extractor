//
//  ATLMessageLinksExtractor.swift
//  ATLMessageContentParser
//
//  Created by Maxim Malyhin on 1/8/16.
//  Copyright © 2016 Maksym Malyhin. All rights reserved.
//

import Foundation

//TODO: There are a lot of code similar to ATLMessageMentionsExtractor in the class. Think if we need to move the logic to separate class/protocol to avoid copy/paste

public class ATLMessageLinksExtractor : ATLMessageContentExtractorInterface
{
    // MARK: - Instance variables

    /*
        For links detection one of popular regular expressions is used. The regular expression with reasonable balance between length and accuracy of detection was selected. Can be changed in future in case of perfomance issues or false positive/negative detections.
    */
    final let linksPattern = "(?<Address>(?<Protocol>https?:\\/\\/)?(?<Subdomain>\\w*\\.)?(?<Domain>(?:[a-z0-9\\-]{2,})\\.(?:[^\\s\\/\\.]{2,}))(?<Path>\\/[^\\s\\?]*)?(?<Params>(?:\\?|\\#)[^\\s\\/\\?\\:]*)?)"
    
    private let linksExtractor: ATLRegularExpressionExtractor?
    private let dispatchQueue = dispatch_queue_create("ATLMessageLinksExtractor.internalQueue", DISPATCH_QUEUE_SERIAL)
    private var titleParsers = Array<ATLHTMLTitleParser>()
    
    // MARK: - Initializers
    
    public init()
    {
        self.linksExtractor = ATLRegularExpressionExtractor(withRegexPattern: self.linksPattern, rangeIndex: 1)
        
        assert(self.linksExtractor != nil, "Could not create linksExtractor")
    }
    
    
    
    // MARK: - ATLMessageContentExtractorInterface
    
    public func extractContentFromString(string: String!, completionHandler:ATLContentExtractingResultHandler!)
    {
        if let linksExtractor = self.linksExtractor {
            
            linksExtractor.extractContentFromString(string, completionHandler: {
                [unowned self] (optionalUrls: [ATLMessageContent]?) -> Void in
                
                //
                
                let opionalUrlStrings = optionalUrls?.map({
                    (content: ATLMessageContent) -> String in
                    
                    switch content {
                        
                    case .StringContent(let urlString):
                        return urlString
                        
                    case .Link(let linkInfo):
                        return linkInfo.URL
                    
                    }
                })
                
                if let urlStrings = opionalUrlStrings {
                    
                    //Create dispatch group perform completion when all links info will be downloaded
                    let htmlPageLoadingGroup = dispatch_group_create()
                    
                    //Array to store downloaded and parsed information about a link
                    var linkInfoArray: [ATLMessageContent] = Array<ATLMessageContent>()
                    linkInfoArray.reserveCapacity(urlStrings.count)
                    
                    for urlString in urlStrings {
                        
                        if let url = NSURL.init(string: urlString)
                        {
                            dispatch_group_enter(htmlPageLoadingGroup)
                            
                            self.downloadInfoForLink(withURL: url, handler: {
                                [unowned self] (optionalLinkInfo: ATLMessageContent?) -> Void in
                                
                                dispatch_async(self.dispatchQueue, {
                                    () -> Void in
                                    
                                    if  let linkInfo = optionalLinkInfo {
                                        
                                        linkInfoArray.append(linkInfo)
                                        
                                        dispatch_group_leave(htmlPageLoadingGroup)
                                        
                                    }
                                })
                            })
                            
                        }
                    }
                    
                    dispatch_group_notify(htmlPageLoadingGroup, self.dispatchQueue, {
                        [unowned self]() -> Void in
                        
                        self.titleParsers.removeAll()
                        completionHandler(linkInfoArray)
                    })
                    
                }
                else {
                    completionHandler(nil);
                }
                
                
            })
            
        }
    }
    
    private func downloadInfoForLink(withURL url: NSURL, handler:(ATLMessageContent?) -> Void)
    {
    
        dispatch_async(self.dispatchQueue) {
            [unowned self]() -> Void in
            
            let titleParser = ATLHTMLTitleParser.init(withPageURL: url, urlSession: nil) {
                (optionalTitle: String?) -> Void in
                
                handler(ATLMessageContent.Link(ATLLinkInfo.init(withURL: url.absoluteString, title: optionalTitle)))
                
            }
            
            self.titleParsers.append(titleParser)
            
            titleParser.parse()
        }
    }
    
}