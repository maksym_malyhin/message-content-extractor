//
//  ALTMessageEmoticonsExtractor.swift
//  ATLMessageContentParser
//
//  Created by Maxim Malyhin on 1/8/16.
//  Copyright © 2016 Maksym Malyhin. All rights reserved.
//

import Foundation

//TODO: There are a lot of code similar to ATLMessageMentionsExtractor in the class. Think if we need to move the logic to separate class/protocol to avoid copy/paste

public class ALTMessageEmoticonsExtractor : ATLMessageContentExtractorInterface
{
    // MARK: - Instance variables
    
    final let mentionsRegexpPattern = "\\(([A-Za-z0-9]{1,15})\\)"
    
    private let emoticonsExtractor: ATLRegularExpressionExtractor?
    
    // MARK: - Initializers
    
    public init()
    {
        self.emoticonsExtractor = ATLRegularExpressionExtractor(withRegexPattern: self.mentionsRegexpPattern, rangeIndex: 1)
        
        assert(self.emoticonsExtractor != nil, "Could not create emoticonsExtractor")
    }
    
    
    // MARK: - ATLMessageContentExtractorInterface
    
    public func extractContentFromString(string: String!, completionHandler:ATLContentExtractingResultHandler!)
    {
        if self.emoticonsExtractor?.extractContentFromString(string, completionHandler: completionHandler) == nil {
            completionHandler(nil)
        }
    }

}