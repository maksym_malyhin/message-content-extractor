//
//  ATLHTMLTitleLibXMLSAXParser.swift
//  ATLMessageContentParser
//
//  Created by Maxim Malyhin on 1/16/16.
//  Copyright © 2016 Maksym Malyhin. All rights reserved.
//


/* The class ATLHTMLTitleLibXMLSAXParser is responsible for parsing content of <title> tag from an HTML web page data.

There are several options to perform this task:

1. Regular expression can be used. Advantage of the method - it is easy to implement. Main disadvantage - comments like "<!-- <title>...</title> -->" can easily break simple regex. It is not possible to handle all cases with regex.

2. Using of UIWebView to parse HTML and request value of "title" attribute by evaluating of JavaScript.
Advantages:
- using of well tested UIWebView to parse HTML will help to avoid error, the implementation will be easy.
Disadvantages
- UIWebView performs not only parsing, but also images and resources downloading, rendering, caching, etc. There is no API to control all these activities, so eventually we can have really poor performance;
- debugging of JavaScript can be tricky.

3. Using of an XML parser to find proper attribute.
Advantages:
- this approach allows to process any valid HTML page;
- in case of using SAX XML parser parsing can be really effective in terms of time complexity and memory usage;
Disadvantages:
- this approach requires the biggest amount of code to implement it. It means that additional time on implementing and testing will be spent.

DESISION:
Taking into account all advantages and disadvantages above, I decided to implement SAX XML parser to extract value of <title> tag.

*/

import Foundation

import libxml2

enum ATLHTMLTitleParsingState
{
    case Initial
    case HTMLTagOpen
    case HeadTagOpen
    case TitleTagOpen
    case TitleTagClose
    case HeadTagClose
    case HTMLTagClose
    case Finished
}

enum ATLHTMLTag : String
{
    case html
    case head
    case title
}


// http://stackoverflow.com/questions/33294620/how-to-cast-self-to-unsafemutablepointervoid-type-in-swift

private func bridge<T : AnyObject>(obj : T) -> UnsafeMutablePointer<Void> {
    return UnsafeMutablePointer(Unmanaged.passUnretained(obj).toOpaque())
}

private func bridge<T : AnyObject>(ptr : UnsafePointer<Void>) -> T {
    return Unmanaged<T>.fromOpaque(COpaquePointer(ptr)).takeUnretainedValue()
}

private extension String {
    static func stringWithXMLString(xmlString: UnsafePointer<xmlChar>) -> String?
    {
        return String.init(CString: UnsafePointer<CChar>(xmlString), encoding: NSUTF8StringEncoding)
    }
}

class ATLHTMLTitleLibXMLSAXParser
{
    private let htmlData: NSData!
    private let completionHandler: (String?) -> Void
    
    private var saxParserHandler: htmlSAXHandler?
    
    private var parsingState: ATLHTMLTitleParsingState = .Initial
    private var htmlDocumentTitleValue: String? = nil
    
    init!(withHTMLPageData data: NSData!, completionHandler: (String?) -> Void)
    {
        self.htmlData = data
        self.completionHandler = completionHandler
    }
    
    func createHTMLSAXHandler() -> htmlSAXHandler
    {
        var saxHandler = htmlSAXHandler()
        
        //Setup handlers
        
        //startDocument
        let startDocumentFunction: @convention(c) (UnsafeMutablePointer<Void>) -> Void  = {
            (context) -> Void in
            
            let contextSelf : ATLHTMLTitleLibXMLSAXParser = bridge(context)
            contextSelf.parserDidStartDocument()
        }
        saxHandler.startDocument = startDocumentFunction
        
        //startElement
        let startElementFunction: @convention(c) (UnsafeMutablePointer<Void>, UnsafePointer<xmlChar>, UnsafeMutablePointer<UnsafePointer<xmlChar>>) -> Void = {
            (context, name, attributes) in
            
            let contextSelf : ATLHTMLTitleLibXMLSAXParser = bridge(context)
            contextSelf.parserDidStartElement(name, attributes: attributes)
        }
        saxHandler.startElement = startElementFunction
        
        //characters
        let charactersFunction: @convention(c) (UnsafeMutablePointer<Void>, UnsafePointer<xmlChar>, CInt) -> Void = {
            (context, characters, lenght) in
            
            let contextSelf : ATLHTMLTitleLibXMLSAXParser = bridge(context)
            contextSelf.parserFoundCharacters(characters, length: lenght)
        }
        saxHandler.characters = charactersFunction
        
        //endElement
        let endElementFunction: @convention(c) (UnsafeMutablePointer<Void>, UnsafePointer<xmlChar>) -> Void = {
            (context, name) in
            
            let contextSelf : ATLHTMLTitleLibXMLSAXParser = bridge(context)
            contextSelf.parserDidEndElement(name)
        }
        saxHandler.endElement = endElementFunction
        
        //endDocument
        let endDocumentFunction: @convention(c) (UnsafeMutablePointer<Void>) -> Void  = {
            (context) -> Void in
            
            let contextSelf : ATLHTMLTitleLibXMLSAXParser = bridge(context)
            contextSelf.parserDidEndDocument()
        }
        saxHandler.endDocument = endDocumentFunction
        
        //Error
        let errorHandler: @convention(c) (UnsafeMutablePointer<Void>, UnsafeMutablePointer<xmlError>) -> Void  = {
            (context, error) -> Void in
        
            let contextSelf : ATLHTMLTitleLibXMLSAXParser = bridge(context)
            contextSelf.parserError(error)
        }
        
        saxHandler.serror = errorHandler
        
        return saxHandler
    }
    
    func parse() -> String?
    {
        let bytes = UnsafeMutablePointer<xmlChar>(self.htmlData.bytes)
        
        var handler = self.createHTMLSAXHandler()
        self.saxParserHandler = handler
        let selfPointer: UnsafeMutablePointer<Void> = bridge(self)
        
        htmlSAXParseDoc(bytes, nil, &handler, selfPointer)
        
        return self.htmlDocumentTitleValue
    }
    
    //TODO: Stop HTML parsing as soon as we found title tag of the HTML document
    func abortParsingBecauseOfError()
    {   
        if (self.parsingState != .Finished) {
            
            self.parsingState = .Finished
            self.completionHandler(nil)
        }
        
    }
    
    func abortParsing(withTitle title: String?)
    {
        if (self.parsingState != .Finished) {
            
            self.parsingState = .Finished
            self.completionHandler(title)
        }
    }
    
    // MARK: - NSXMLParserDelegate
    
    func parserDidStartDocument()
    {
        
    }
    
    func parserDidStartElement (elementName: UnsafePointer<xmlChar>, attributes: UnsafeMutablePointer<UnsafePointer<xmlChar>>)
    {
        if let name = String.stringWithXMLString(elementName) {
            
            if let htmlTag = ATLHTMLTag(rawValue: name) {
                
                switch htmlTag {
                    
                case .html:
                    switch self.parsingState {
                        
                    case .Initial:
                        self.parsingState = .HTMLTagOpen
                        
                    default:
                        self.abortParsingBecauseOfError()
                    }
                    
                case .head:
                    switch self.parsingState {
                        
                    case .HTMLTagOpen:
                        self.parsingState = .HeadTagOpen
                        
                    default:
                        self.abortParsingBecauseOfError()
                    }
                    
                case .title:
                    switch self.parsingState {
                        
                    case .HeadTagOpen:
                        self.parsingState = .TitleTagOpen
                        
                    default:
                        self.abortParsingBecauseOfError()
                        
                    }
                }
            }
        }
    }
    
    func parserFoundCharacters(characters: UnsafePointer<xmlChar>, length: CInt)
    {
        if let string = String.stringWithXMLString(characters) {
            
            if self.parsingState == .TitleTagOpen {
                
                if var currentTitle = self.htmlDocumentTitleValue {
                    
                    currentTitle += string
                    self.htmlDocumentTitleValue = currentTitle
                }
                else {
                    self.htmlDocumentTitleValue = string
                }
            }
        }
    }
    
    func parserDidEndElement(elementName: UnsafePointer<xmlChar>)
    {
        if let name = String.stringWithXMLString(elementName) {
            
            if let htmlTag = ATLHTMLTag(rawValue: name) {
                
                switch htmlTag {
                    
                case .title:
                    switch self.parsingState {
                        
                    case .TitleTagOpen:
                        self.abortParsing(withTitle: self.htmlDocumentTitleValue)
                        
                    default:
                        self.abortParsingBecauseOfError()
                        
                    }
                    
                default:
                    self.abortParsingBecauseOfError()
                    
                }
            }
        }
    }
    
    func parserDidEndDocument()
    {
        self.parsingState = .Finished
        self.completionHandler(self.htmlDocumentTitleValue)
    }
    
    
    func parserError(error: UnsafeMutablePointer<xmlError>)
    {
        if self.parsingState != .Finished {
            self.abortParsingBecauseOfError()
        }
        
        print("XML parsing error")
    }
    
}