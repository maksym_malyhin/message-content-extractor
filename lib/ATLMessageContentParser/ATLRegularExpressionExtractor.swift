//
//  ALTRegexExtractor.swift
//  ATLMessageContentParser
//
//  Created by Maxim Malyhin on 1/8/16.
//  Copyright © 2016 Maksym Malyhin. All rights reserved.
//

import Foundation

class ATLRegularExpressionExtractor : ATLMessageContentExtractorInterface
{
    
    // MARK: Instance variables
    let regularExpression: NSRegularExpression?
    let rangeIndex: Int
    
    // MARK: Initializers
    
    init?(withRegexPattern pattern: String, rangeIndex: Int)
    {
        self.rangeIndex = rangeIndex;
        
        do {
            self.regularExpression = try NSRegularExpression(pattern:pattern, options: [])
        }
        catch {
            self.regularExpression = nil
            return nil
        }
    }
    
    
    // MARK: - ATLMessageContentExtractorInterface
    
    func extractContentFromString(string: String!, completionHandler:ATLContentExtractingResultHandler!)
    {
        let nsString = string as NSString
        let matchesRanges = (self.regularExpression?.matchesInString(string, options: [], range: NSMakeRange(0, nsString.length)))! as [NSTextCheckingResult]
        
        if (matchesRanges.count > 0) {
            let matches = matchesRanges.map({
                (results: NSTextCheckingResult) -> ATLMessageContent in
                
                let matchString = nsString.substringWithRange(results.rangeAtIndex(self.rangeIndex))
                return ATLMessageContent.StringContent(matchString)
            })
            completionHandler(matches);
        }
        else {
            completionHandler(nil)
        }
    }
    
}