//
//  libxml2-atl.h
//  ATLMessageContentParser
//
//  Created by Maxim Malyhin on 1/16/16.
//  Copyright © 2016 Maksym Malyhin. All rights reserved.
//

#ifndef libxml2_atl_h
#define libxml2_atl_h

#import <libxml2/libxml/parser.h>
#import <libxml2/libxml/HTMLparser.h>

#endif /* libxml2_atl_h */
