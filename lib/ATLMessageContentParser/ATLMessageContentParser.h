//
//  ATLMessageContentParser.h
//  ATLMessageContentParser
//
//  Created by Maxim Malyhin on 1/7/16.
//  Copyright © 2016 Maksym Malyhin. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for ATLMessageContentParser.
FOUNDATION_EXPORT double ATLMessageContentParserVersionNumber;

//! Project version string for ATLMessageContentParser.
FOUNDATION_EXPORT const unsigned char ATLMessageContentParserVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ATLMessageContentParser/PublicHeader.h>


