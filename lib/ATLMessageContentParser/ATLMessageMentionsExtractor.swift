//
//  ATLMessageMentionsExtractor.swift
//  ATLMessageContentParser
//
//  Created by Maxim Malyhin on 1/7/16.
//  Copyright © 2016 Maksym Malyhin. All rights reserved.
//

import Foundation

public class ATLMessageMentionsExtractor : ATLMessageContentExtractorInterface
{
    // MARK: - Instance variables
    
    final let mentionsRegexpPattern = "@([A-Za-z0-9]+)"

    private let mentionsExtractor: ATLRegularExpressionExtractor?
    
    // MARK: - Initializers
    
    public init()
    {
        self.mentionsExtractor = ATLRegularExpressionExtractor(withRegexPattern: self.mentionsRegexpPattern, rangeIndex: 1)
        
        assert(self.mentionsExtractor != nil, "Could not create mentionsExtractor")
    }
    
    
    // MARK: - ATLMessageContentExtractorInterface
    
    public func extractContentFromString(string: String!, completionHandler:ATLContentExtractingResultHandler!)
    {
        if self.mentionsExtractor?.extractContentFromString(string, completionHandler: completionHandler) == nil {
            completionHandler(nil)
        }
    }
}