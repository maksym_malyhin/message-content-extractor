//
//  ATLHTMLTitleWebViewBasedParser.swift
//  ATLMessageContentParser
//
//  Created by Maxim Malyhin on 1/10/16.
//  Copyright © 2016 Maksym Malyhin. All rights reserved.
//

// The HTML document parser based on WKWebKit can be used as reference solution to check correctness of other parser implementations. It has poor performance, but it treats an HTML document as modern browsers, so can handle all modern HTML syntax

import Foundation
import WebKit


class ATLHTMLTitleWebViewBasedParser : NSObject, WKNavigationDelegate
{
    let pageURL: NSURL
    var webView: WKWebView?
    var completionHandler: (String?) -> Void
    
    init(withPageURL pageURL: NSURL, completionHandler: (String?) -> Void)
    {
        self.pageURL = pageURL
        self.completionHandler = completionHandler
        
        super.init()
        
        print("--ATLHTMLTitleWebViewBasedParser init: \(pageURL), \(self)")
    }
    
    
    func parse()
    {
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.webView = WKWebView.init()
            self.webView?.navigationDelegate = self
            
            self.webView?.loadRequest(NSURLRequest.init(URL: self.pageURL))
        }
    }
    
    
    func complete(title: String?)
    {
        self.completionHandler(title)
        self.webView?.navigationDelegate = nil
        self.webView = nil
    }
    
    // MARK: UIWebViewDelegate
    
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!)
    {
        webView.evaluateJavaScript("document.title") {
            (result: AnyObject?, error: NSError?) -> Void in
            
            self.complete(result as? String)
        }
        
        print("--ATLHTMLTitleWebViewBasedParser didFinishNavigation: \(self.pageURL), \(self)")
    }
    
    func webView(webView: WKWebView, didFailNavigation navigation: WKNavigation!, withError error: NSError)
    {
        self.complete(nil)
        
    }
}