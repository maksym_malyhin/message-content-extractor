//
//  ATLContentFromNetworkParsingTests.swift
//  ATLMessageContentParser
//
//  Created by Maxim Malyhin on 1/17/16.
//  Copyright © 2016 Maksym Malyhin. All rights reserved.
//

import XCTest

@testable import ATLMessageContentParser

class ATLContentFromNetworkParsingTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testContentExtracting()
    {
        let message = "@bob (succ_es+s) such (stringLongerThan15Characters) a cool feature; @john (coffee) http://maksym_malyhin.bitbucket.org/real_world.html . @max please take a look at the link http://maksym_malyhin.bitbucket.org/simple.html (emoticon)"
        
        let linksKey = "links"
        let emoticonsKey = "emoticons"
        let mentionsKey = "mentions"
        
        let expectingMentions = ["bob", "john", "max"]
        let expectingEmoticons = ["coffee", "emoticon"]
        
        // Symbols "/" are escaped by "\" in order to handle a JSON serialization issue. See ATLMessageContentExtractor.extractContentFromString() comments
        let expectingLinkInfo =
        [
            [
                "url": "http://maksym_malyhin.bitbucket.org/simple.html",
                "title": "An Example Page"
            ],
            [
                "url" : "http://maksym_malyhin.bitbucket.org/real_world.html",
                "title" : "Justin Dorfman on Twitter: \"nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq\""
            ],
        ]
        
        
        
        
        let contentExtractor = ATLMessageContentExtractor(withContentExtractorsByKey:
            [
                mentionsKey : ATLMessageMentionsExtractor(),
                emoticonsKey : ALTMessageEmoticonsExtractor(),
                linksKey : ATLMessageLinksExtractor()
            ])
        
        let completionHandlerExpectation = self.expectationWithDescription("Completion handler called")
        
        contentExtractor.extractContentFromString(message) {
            (optionalJSONString: String?) -> Void in
            
            completionHandlerExpectation.fulfill()
            
            if let jsonString = optionalJSONString {
                
                do {
                    let optionalJSONObject = try NSJSONSerialization.JSONObjectWithData(jsonString.dataUsingEncoding(NSUTF8StringEncoding)!, options: []) as? [String : [AnyObject]]
                    
                    if let jsonObject = optionalJSONObject {
                        
                        //Check mentions
                        let optionalMentions = jsonObject[mentionsKey] as? [String]
                        if let mentions = optionalMentions {
                            
                            XCTAssertEqual(mentions, expectingMentions)
                        }
                        else {
                            XCTAssertTrue(false, "Mentions were not parsed")
                        }
                        
                        //Check emoticons
                        let optionalEmoticons = jsonObject[emoticonsKey] as? [String]
                        
                        if let emoticons = optionalEmoticons  {
                            
                            XCTAssertEqual(emoticons, expectingEmoticons)
                        }
                        else {
                            XCTAssertTrue(false, "Emoticons were not parsed")
                        }
                        
                        //Check links
                        let optionalLinks = jsonObject[linksKey]
                        
                        if  let links = optionalLinks as? [Dictionary<String, String>]  {
                            
                            XCTAssertEqual(links, expectingLinkInfo)
                        }
                        else {
                            XCTAssertTrue(false, "Links were not parsed")
                        }
                    }
                    
                }
                catch {
                    
                    XCTAssertTrue(false, "The string is a not valid JSON")
                }
                
            }
            else {
                XCTAssertTrue(false, "JSON has not been created.")
            }
        }
        
        self.waitForExpectationsWithTimeout(30) {
            (error: NSError?) -> Void in
            
            
        }
    }

}
