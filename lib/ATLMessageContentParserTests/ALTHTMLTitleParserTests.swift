//
//  ALTHTMLTitleParserTests.swift
//  ATLMessageContentParser
//
//  Created by Maxim Malyhin on 1/10/16.
//  Copyright © 2016 Maksym Malyhin. All rights reserved.
//

import Foundation

import XCTest
@testable import ATLMessageContentParser

class ALTHTMLTitleParserTests: XCTestCase
{
    
    func testHTMLTitleParser()
    {
        //Simplest HTML page
        let expectingTitle1 = "An Example Page"
        
        if let htmlURL1 = NSBundle(forClass: self.dynamicType).URLForResource("simple", withExtension: "html") {
            
            self.parseHTMLTitle(htmlURL1, expectedTitle: expectingTitle1)
        }
        
        //Real world HTML page
        let expectingTitle2 = "Justin Dorfman on Twitter: \"nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq\""
        
        if let htmlURL2 = NSBundle(forClass: self.dynamicType).URLForResource("real_world", withExtension: "html") {
            
            self.parseHTMLTitle(htmlURL2, expectedTitle: expectingTitle2)
        }
        
    }
    
    func parseHTMLTitle(url: NSURL, expectedTitle: String)
    {
        
        let completionHandlerExpectation = self.expectationWithDescription("Completion Handler Expectation")
        
        let parser = ATLHTMLTitleWebViewBasedParser.init(withPageURL: url) {
            (optionalTitle: String?) -> Void in
            
            completionHandlerExpectation.fulfill()
            
            if let title = optionalTitle {
                XCTAssertEqual(title, expectedTitle)
            }
            else {
                XCTAssertTrue(false, "Could not parse title. Expected title: \(expectedTitle)")
            }
        }
        
        parser.parse()
        
        
        self.waitForExpectationsWithTimeout(10, handler: nil)
    }

}