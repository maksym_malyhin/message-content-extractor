//
//  ATLMessageContentParserTests.swift
//  ATLMessageContentParserTests
//
//  Created by Maxim Malyhin on 1/7/16.
//  Copyright © 2016 Maksym Malyhin. All rights reserved.
//

import XCTest
@testable import ATLMessageContentParser


class ATLMessageContentParserTests: XCTestCase {
    
    func testEmoticonsExtracting() {
        
        let string1 = "@bob (success) such a cool feature; @john (coffee) https://twitter.com/jdorfman/status/430511497475670016"
        
        let emoticonsExtractor = ALTMessageEmoticonsExtractor();
        
        XCTAssertNotNil(emoticonsExtractor)
        
        emoticonsExtractor.extractContentFromString(string1, completionHandler: {
            (optionalResult: [ATLMessageContent]?) -> Void in
            
            if let matches = optionalResult {
                XCTAssertEqual(matches, [ATLMessageContent.StringContent("success"), ATLMessageContent.StringContent("coffee")])
            }
            else {
                XCTAssertTrue(false, "No emoticons found")
            }
            
        })
        
        let string2 = "@bob (succ_es+s) such (stringLongerThan15Characters) a cool feature; @john (coffee) https://twitter.com/jdorfman/status/430511497475670016"
        
        emoticonsExtractor.extractContentFromString(string2, completionHandler: {
            (optionalResult: [ATLMessageContent]?) -> Void in
            
            if let matches = optionalResult {
                XCTAssertEqual(matches, [ATLMessageContent.StringContent("coffee")])
            }
            else {
                XCTAssertTrue(false, "No emoticons found")
            }
        })
    }
    
    func testLinksExtracting () {
        
        let expectingTitle1 = "Justin Dorfman on Twitter: \"nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq\""
        
        //TODO: Add support of "file:///..." URLs to implement independent unit tests
        
        let htmlURL1 = NSURL.init(string: "http://maksym_malyhin.bitbucket.org/real_world.html")
        //NSBundle(forClass: self.dynamicType).URLForResource("real_world", withExtension: "html")
        
        if  htmlURL1 == nil {
            XCTAssertTrue(false, "Could not find test HTML files")
            return
        }
        
        let expectedResult =
        [
            ATLMessageContent.Link(ATLLinkInfo.init(withURL: htmlURL1!.absoluteString, title: expectingTitle1)),
        ]
        
        
        let string1 = "@bob (success) such a cool feature; @john_ \(htmlURL1!) @ fake_mention1 @+not_mention"
        
        let linksExtractor = ATLMessageLinksExtractor()
        
        XCTAssertNotNil(linksExtractor)
        
        let completionHandlerExpectation = self.expectationWithDescription("Completion handler was called")
        
        linksExtractor.extractContentFromString(string1, completionHandler: {
            (optionalResult: [ATLMessageContent]?) -> Void in
            
            completionHandlerExpectation.fulfill()
            
            if let result = optionalResult {
                
                XCTAssertEqual(result.count, expectedResult.count)
                XCTAssertEqual(result, expectedResult)
            }
            else {
                XCTAssertTrue(false, "No links found")
            }
            
            
        })
        
        self.waitForExpectationsWithTimeout(30, handler: nil)
    }
    
    
    func testMentionsExtracting() {
        let string1 = "@bob (success) such a cool feature; @john_ https://twitter.com/jdorfman/status/430511497475670016 @ fake_mention1 @+not_mention"
        
        let mentionsExtractor = ATLMessageMentionsExtractor();
        
        XCTAssertNotNil(mentionsExtractor)
        
        mentionsExtractor.extractContentFromString(string1, completionHandler: {
            (optionalResult: [ATLMessageContent]?) -> Void in
            
            if let result = optionalResult {
                
                XCTAssertEqual(result, [ATLMessageContent.StringContent("bob"), ATLMessageContent.StringContent("john")])
                
            }
            else {
                XCTAssertTrue(false, "No mentions found")
            }
        })
    }
}
