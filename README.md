# Project structure #

All required code, libraries and dependencies are collected into a workspace ```ATLMessageContentParser``` [ATLMessageContentParser.workspace](https://bitbucket.org/maksym_malyhin/message-content-extractor/src/978be37a45cce65d137091f9d4b465a3af2d7c95/ATLMessageContentParser.xcworkspace/?at=master). It contains two projects:

1. **ATLMessageContentParser** - main target is ```ATLMessageContentParser.framework```, that encapsulates and provides access to content parsing logic. There are some primitive unit tests to validate correctness.
1. **ATLMessageContentTestApp** - contains code of a simple iOS application that presents JSON representation of content parsed from a string entered by a user.

# Branches #

The final solution is placed in ```main``` branch. Other branches contains intermediate enhancements and can be ignored.

The ```html-reader``` branch contains an attempt to implement an HTML document title parsing using one of open source HTML parsers.