//
//  ATLKeyboardNotificationUtils.swift
//  ATLMessageContentTestApp
//
//  Created by Maxim Malyhin on 1/12/16.
//  Copyright © 2016 Maksym Malyhin. All rights reserved.
//

import Foundation

import UIKit

extension NSNotification {
    
    public func atl_keyboardBeginFrame() -> CGRect?
    {
        return self.atl_CGRectForUserInfoValueForKey(UIKeyboardFrameBeginUserInfoKey)
    }
    
    public func atl_keyboardEndFrame() -> CGRect?
    {
        return self.atl_CGRectForUserInfoValueForKey(UIKeyboardFrameEndUserInfoKey)
    }
    
    public func atl_keyboardTransitionDuration() -> NSTimeInterval?
    {
        let durationNumber = self.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber
        
        if let duration = durationNumber?.doubleValue {
            return duration
        }
        else {
            return nil
        }
    }
    
    public func atl_keyboardAnimationCurve() -> UIViewAnimationCurve?
    {
        let curveNumber = self.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
        
        if let curveIneger = curveNumber?.integerValue
        {
            if let curve = UIViewAnimationCurve.init(rawValue: curveIneger) {
                
                return curve
            }
        }
        
        return nil
    }
    
    private func atl_CGRectForUserInfoValueForKey(key: String!) -> CGRect?
    {
        let frameNSValue = self.userInfo?[key] as? NSValue
        
        if let frame = frameNSValue?.CGRectValue() {
            return frame
        }
        else {
            return nil
        }
    }
}

public extension UIViewAnimationOptions
{
    init(withUIViewAnimationCurve curve: UIViewAnimationCurve)
    {
        self.init()
        
        switch (curve) {
            
        case .EaseIn:
            self.intersectInPlace(.CurveEaseIn)
            
        case .EaseInOut:
            self.intersectInPlace(.CurveEaseInOut)
            
        case .EaseOut:
            self.intersectInPlace(.CurveEaseOut)
            
        case .Linear:
            self.intersectInPlace(.CurveLinear)
            
        }
    }
}