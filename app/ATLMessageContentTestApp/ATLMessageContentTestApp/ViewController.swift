//
//  ViewController.swift
//  ATLMessageContentTestApp
//
//  Created by Maxim Malyhin on 1/12/16.
//  Copyright © 2016 Maksym Malyhin. All rights reserved.
//

import UIKit

import ATLMessageContentParser
import WebKit

enum MessageViewControllerState
{
    case MessageEntering
    case MessageParsing
}

class ViewController: UIViewController {

    @IBOutlet var resultTextView: UITextView!
    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet var messageTextField: UITextField!
    @IBOutlet var parsingButton: UIButton!
    
    @IBOutlet var bottomTextFieldConstraint: NSLayoutConstraint!
    let contentParser = ATLMessageContentExtractor(withContentExtractorsByKey:
        [
            "mentions" : ATLMessageMentionsExtractor.init(),
            "emoticons" : ALTMessageEmoticonsExtractor(),
            "links" : ATLMessageLinksExtractor()
        ])
    
    
    var state = MessageViewControllerState.MessageEntering  {
        
        willSet(controllerState) {
            
            switch (controllerState) {
                
            case .MessageEntering:
                
                self.parsingButton.enabled = true
                self.loadingIndicator.stopAnimating()
                
            case .MessageParsing:
                
                self.parsingButton.enabled = false
                self.loadingIndicator.startAnimating()
                
            }
        }
    }

    
    // MARK: View Controller life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: Selector("keyboardWillShowNotificationReceived:"),
            name: UIKeyboardWillShowNotification,
            object: nil)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: Selector("keyboardWillHideNotificationReceived:"),
            name: UIKeyboardWillHideNotification,
            object: nil)
        
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        
        self.messageTextField.becomeFirstResponder()
    }
    
    override func viewDidDisappear(animated: Bool) {
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    // MARK: Actions

    @IBAction func parsingButtonPressed(sender: UIButton)
    {
        if let message = self.messageTextField.text {
            
            self.parseMessageAndShowResult(message)
            self.messageTextField.text = nil
        }
        
    }
    
    // MARK - 
    
    func parseMessageAndShowResult(message: String!)
    {
        self.state = .MessageParsing
        
        self.contentParser.extractContentFromString(message) {
            [unowned self] (optionalResult: String?) -> Void in
            
            dispatch_async(dispatch_get_main_queue(), {
                [unowned self] () -> Void in
                
                let result = (optionalResult == nil) ? "" : optionalResult!
                
                self.resultTextView.text = "Message: \n \(message) \nContent: \n\(result)"
                self.state = .MessageEntering
            })
            
            
        }
    }
    
    // MARK: Keyboard
    
    func keyboardWillShowNotificationReceived(notification: NSNotification)
    {
        if let keyboardFrame = notification.atl_keyboardEndFrame() {
            
            if let animationDuration = notification.atl_keyboardTransitionDuration() {
                
                if let animationCurve = notification.atl_keyboardAnimationCurve() {
                    
                    UIView.animateWithDuration(animationDuration,
                        delay: 0,
                        options: UIViewAnimationOptions.init(withUIViewAnimationCurve: animationCurve),
                        animations: { () -> Void in
                            
                            let keyboardHeight = CGRectGetHeight(keyboardFrame)
                            self.bottomTextFieldConstraint.constant = keyboardHeight + 10
                            
                            self.view.layoutIfNeeded()
                        },
                        completion: nil)
                }
                
            }
        }
    }
    
    func keyboardWillHideNotificationReceived(notification: NSNotification)
    {
        if let animationDuration = notification.atl_keyboardTransitionDuration() {
            
            UIView.animateWithDuration(animationDuration, animations: {
                () -> Void in
                
                self.bottomTextFieldConstraint.constant = 0
            })
        }
    }
    
}

